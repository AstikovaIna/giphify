# Giphify Project
<img src="./giphify/images/Baner.jpg"/>
### You can view the project in a browser here 👉(https://astikovaina.gitlab.io/giphify/giphify/)

The **Giphiphy** project is a single page application (SPA) using **GIPHY** API Key.
## Table of contents
- [Giphify Project](#giphify-project)
  - [Table of contents](#table-of-contents)
  - [What the project does](#what-the-project-does)
  - [Visuals](#visuals)
  - [What's included](#whats-included)
  - [Dependencies](#dependencies)
  - [Categories](#categories)
  - [Browser Support](#browser-support)
  - [Authors](#authors)
  - [Used technologies](#used-technologies)
  - [Copyright and license](#copyright-and-license)

## What the project does
- It shows trending gif's, gif's details when hoover on gif, uploaded gif's, you can upload gif's and you can search gif's by name.


## Visuals
- Home page;
<img src="./giphify/images/home-page.jpg"/>
- Trending;
<img src="./giphify/images/trending-page.jpg"/>
- Favorites;
<img src="./giphify/images/favorites.jpg"/>
- Uploaded;
<img src="./giphify/images/uploaded.jpg"/>
- Upload;
<img src="./giphify/images/upload-gif.jpg"/>
- Search;
<img src="./giphify/images/search.jpg"/>

## What's included
  The project is wrote on Visual Studio Code and using:
- Javascript;
- HTML;
- CSS;
- ESLint;
- GIT;
- ECMAScript features;

## Dependencies
- "babel-eslint": "^10.1.0",
- "eslint": "^8.13.0",
- "eslint-config-google": "^0.14.0"

## Categories
- Home
- Trending
- Favorites 
- Uploaded
- Upload

## Browser Support
At the moment, we aim to support all major web browsers. Any issue in the browsers listed below should be reported as a bug:
- Internet Explorer 10+
- Microsoft Edge 14+
- Safari 6+
- Firefox  Current versions
- Chrome   Current versions
- Opera    Current versions
- Safari iOS 7.0+
- Android 6.0+

## Authors 
- [Velina Astikova](https://gitlab.com/AstikovaIna)
- [Nikola Stanilov](https://gitlab.com/nikolastanilov)
- [Nikola Lyutsov](https://gitlab.com/NikolaLyutsov)
## Used technologies
- HTML
- CSS
- JAVASCRIPT

## Copyright and license

Code and documentation copyright 2022  [MIT License]
