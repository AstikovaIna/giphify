import { loadGifs } from '../common/request-data.js';
import { SEARCH_API } from '../common/constants.js';
import { showSingleGif } from '../views/gif-views.js';

export const searchGifsEventHandler = () => {
  const input = document.querySelector('#search');
  const searchTerm = input.value;
  const url = SEARCH_API.concat(searchTerm);
  loadGifs(url)
    .then((res) => {
      const html = res.data.map(showSingleGif);
      const resultDiv = document.querySelector('#container');
      resultDiv.innerHTML = html;
    })
    .catch((err) => {
      console.log(err);
    });
};
