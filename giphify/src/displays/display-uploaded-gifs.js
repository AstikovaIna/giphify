import { GET_UPLOAD_URL } from '../common/constants.js';
import { showSingleGif } from '../views/gif-views.js';
import { getUploads } from '../events/upload-events-local-storage.js';
import { NO_UPLOADED_ITEMS } from '../common/constants.js';
export const uploadedItemEventHandler = async () => {

  const gifIds = getUploads();
  const requestURL = GET_UPLOAD_URL.concat(gifIds);
  const resultDiv = document.querySelector('#container');

  if (gifIds.length === 0) {
    return (resultDiv.innerHTML = `<span class="errors">${NO_UPLOADED_ITEMS}</span>`);
  } else {
    try {
      const response = await fetch(requestURL);
      const result = await response.json();

      const html = result.data.map(showSingleGif);
      return (resultDiv.innerHTML = html);
    } catch (error) {
      throw new Error(error.message);
    }
  }
};
