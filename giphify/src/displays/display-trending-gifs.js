import { loadGifs } from '../common/request-data.js';
import { API_TRENDING } from '../common/constants.js';
import { showSingleGif } from '../views/gif-views.js';

export const trendingGifsEventHandler = () => {
  loadGifs(API_TRENDING)
    .then((res) => {
      const html = res.data.map(showSingleGif);
      const resultDiv = document.querySelector('#container');
      resultDiv.innerHTML = html;
    })
    .catch((err) => {
      console.log(err);
    });
};
