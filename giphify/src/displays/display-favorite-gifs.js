import { getFavorites } from '../events/favorite-events-local-storage.js';
import { loadGifs } from '../common/request-data.js';
import { RANDOM_API } from '../common/constants.js';
import { showSingleGif } from '../views/gif-views.js';
export const favoriteGifsEventHandler = () => {
  const idGifs = getFavorites();
  if (idGifs.length === 0) {
    loadGifs(RANDOM_API)
      .then((res) => {
        const html = showSingleGif(res.data);
        const resultDiv = document.querySelector('#container');
        resultDiv.innerHTML = html;
      })
      .catch((err) => {
        console.log(err);
      });
  }
  loadGifs(
    `http://api.giphy.com/v1/gifs?ids=${idGifs}&api_key=Yrruw5gKDptt4x5JJGsBsODW7B5Pw9TP`,
  )
    .then((res) => {
      const html = res.data.map(showSingleGif);
      const resultDiv = document.querySelector('#container');
      resultDiv.innerHTML = html;
    })
    .catch((err) => {
      console.log(err);
    });
};
