const getGifs = async (url) => {
  const res = await fetch(url).then((result) => result.json());
  return res;
};

export const loadGifs = (url) => getGifs(url);
