import { renderFavoriteStatus } from '../events/favorite-events-local-storage.js';
export const showSingleGif = (gif) => {
  const { title, id, username, type, images, url } = gif;
  const gifEl = document.createElement('div');
  gifEl.classList.add('gif');

  return ` 
  <div class="gif">   
         <img src="${images.downsized.url}" alt="${title}">
         <div class="gif-details">
          <h3>${title}</h3>
        
         </div>
      <div class="details">
        <h3>Details</h3>
        username: ${username} <br>
        type:${type} <br>
        <a href="${url}" target="_blank">source</a>
        <span class="gif-details span">${renderFavoriteStatus(id)}</span>
      </div>

    </div>

  `;
};
