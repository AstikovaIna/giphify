export const renderUploadView = () => {
  const resultDiv = document.querySelector('#container');
  const uploadBtn = `<form id="upload-form">
    <input id="file-upload" type="file" name="attachment">
    <button type="submit" id="upload-btn">Upload</button>
  </form>`;
  resultDiv.innerHTML = uploadBtn;
};
