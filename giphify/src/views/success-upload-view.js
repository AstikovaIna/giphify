import { UPLOADED_SUCCESS } from '../common/constants.js';
export const renderUploadDoneView = () => {
  const resultDiv = document.querySelector('#container');
  const uploadBtn = `
  <span class="upload-done">${UPLOADED_SUCCESS}</span>`;
  resultDiv.innerHTML = uploadBtn;
};
