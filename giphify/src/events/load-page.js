import { favoriteGifsEventHandler } from '../displays/display-favorite-gifs.js';
import { trendingGifsEventHandler } from '../displays/display-trending-gifs.js';
import { uploadedItemEventHandler } from '../displays/display-uploaded-gifs.js';
// eslint-disable-next-line consistent-return
export const loadPage = (page = '') => {
  switch (page) {
  case 'home-nav':
    return location.reload();

  case 'trending-nav':
    return trendingGifsEventHandler();

  case 'favorites-nav':
    return favoriteGifsEventHandler();

  case 'uploaded-nav':
    return uploadedItemEventHandler();
  }
};
