import { FULL_HEART } from '../common/constants.js';
import { EMPTY_HEART } from '../common/constants.js';
let favorites = JSON.parse(localStorage.getItem('favorites')) || [];
export const renderFavoriteStatus = (gifId) => {
  const fav = getFavorites();
  return fav.includes(gifId) ?
    `<span class="fav-heart" data-gifId="${gifId}">${FULL_HEART}</span>` :
    `<span class="fav-heart" data-gifId="${gifId}">${EMPTY_HEART}</span>`;
};
export const getFavorites = () => [...favorites];

const addFavorite = (gifId) => {
  if (favorites.find((id) => id === gifId)) {
    return;
  }
  favorites.push(gifId);
  localStorage.setItem('favorites', JSON.stringify(favorites));
};

const removeFavorite = (gifId) => {
  favorites = favorites.filter((id) => id !== gifId);
  localStorage.setItem('favorites', JSON.stringify(favorites));
};
export const toggleFavoriteStatus = (gifId) => {
  const fav = getFavorites();

  if (fav.includes(gifId)) {
    removeFavorite(gifId);
  } else {
    addFavorite(gifId);
  }
  document.querySelector(`span[data-gifId="${gifId}"]`).innerHTML =
    renderFavoriteStatus(gifId);
};
