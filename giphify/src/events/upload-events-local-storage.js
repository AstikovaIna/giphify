const uploads = JSON.parse(localStorage.getItem('uploadedGif')) || [];

export const addUploadGif = (uplGifId) => {
  if (uploads.find((id) => id === uplGifId)) {
    return;
  }

  uploads.push(uplGifId);
  localStorage.setItem('uploadedGif', JSON.stringify(uploads));
};

export const getUploads = () => [...uploads];

export const toggleUploadItems = (uplGifId) => {
  const upload = getUploads();

  if (!upload.includes(uplGifId)) {
    addUploadGif(uplGifId);
    return;
  }
};
