import { UPLOAD_URL } from '../common/constants.js';
import { renderUploadDoneView } from '../views/success-upload-view.js';
import { toggleUploadItems } from './upload-events-local-storage.js';
import { NO_NAMED_FILE } from '../common/constants.js';
export const getResponse = async (file) => {
  const newForm = new FormData();
  newForm.append('file', file);

  if (newForm.get('file').name === '') {
    return `<span class="errors">${NO_NAMED_FILE}</span>`;
  }

  try {
    const response = await fetch(`${UPLOAD_URL}`, {
      method: 'POST',
      body: newForm,
    });
    const result = await response.json();

    toggleUploadItems(result.data.id);

    return renderUploadDoneView();

  } catch (error) {
    throw new Error(`${error.message}`);
  }
};
