import { loadPage } from './events/load-page.js';
import { searchGifsEventHandler } from './displays/display-searched-gifs.js';
import { renderUploadView } from './views/upload-form-view.js';
import { getResponse } from './events/upload-gif-function.js';
import { toggleFavoriteStatus } from './events/favorite-events-local-storage.js';

document.addEventListener('DOMContentLoaded', function() {
  document.getElementById('search').addEventListener('input', (e) => {
    e.preventDefault();
    searchGifsEventHandler();
  });

  document.addEventListener('click', (e) => {
    if (e.target?.classList.contains('nav')) {
      loadPage(e.target.getAttribute('data-page'));
    }
    if (e.target?.classList.contains('fav-heart')) {
      toggleFavoriteStatus(e.target.getAttribute('data-gifId'));
    }
    if (e.target?.classList.contains('button')) {
      renderUploadView();
      document.querySelector('#upload-form').addEventListener('submit', (event) => {
        event.preventDefault();
        getResponse(event.target.attachment.files[0]);
      });
    }
  });
});
